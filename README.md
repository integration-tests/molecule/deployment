#

```bash
ansible-galaxy collection install --requirements-file requirements/collections.yml --force
ansible-galaxy role install --role-file requirements/roles.yml  --force
```

```bash
ansible-playbook --inventory inventories/docker/inventory.yml  playbooks/molecule.yml --diff --check
```
